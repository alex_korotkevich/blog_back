import busboy from "busboy";
import { Request, Response, Router } from "express";
import fs from "fs";
import path from "path";
import { authLoginMiddleware } from "../middleware/auth-login-middleware";
import { authMiddleware, TAuthRequest } from "../middleware/auth-middleware";
import { authRegistrationMiddleware } from "../middleware/auth-registration-middleware";
import { TCreateAuthor } from "../types/author-create";
import { generateToken } from "../util/generate-token";
import { authService } from "./auth-service";

export const authRouter = Router();

authRouter.post(
  "/login",
  [authLoginMiddleware],
  async (req: Request, res: Response) => {
    try {
      const login = req.body.login as string;
      const password = req.body.password as string;
      const token = await authService.login(login, password);
      res.send(token);
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.post(
  "/registration",
  [authRegistrationMiddleware],
  async (req: Request, res: Response) => {
    try {
      const data = req.body as TCreateAuthor;
      await authService.registration(data);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.get(
  "/logout",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const authorId = (req as TAuthRequest).userId;
      await authService.logout(authorId);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.get("/me", [authMiddleware], async (req: Request, res: Response) => {
  try {
    const userId = (req as TAuthRequest).userId;
    const user = await authService.me(userId);
    res.send(user);
  } catch (err) {
    res.status(403).send((err as Error).message);
  }
});

authRouter.put(
  "/upload",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const userId = (req as TAuthRequest).userId;
      const stream = busboy({ headers: req.headers });
      let fileName = generateToken();

      stream.on(
        "file",
        function (
          _fieldname: string,
          file: NodeJS.ReadableStream,
          fileData: any,
          _encoding: string,
          _mimetype: string
        ) {
          const type = fileData.filename.split(".").pop();
          fileName = `${fileName}.${type}`;
          const saveTo = path.resolve(
            process.env.FRONTEND_PUBLIC_PATH || "",
            "uploads",
            fileName
          );
          file.pipe(fs.createWriteStream(saveTo));
        }
      );

      stream.on("finish", async function () {
        await authService.upload(userId, fileName);
        res.writeHead(200, { Connection: "close" });
        res.end("That's all folks!");
      });

      return req.pipe(stream);
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);
