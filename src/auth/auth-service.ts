import { AuthorToken } from "../entity/author-token-entity";
import { Comment } from "../entity/comment-entity";
import { Post } from "../entity/post-entity";
import { User } from "../entity/user-entity";
import { ormClient } from "../orm-client";
import { TCreateAuthor } from "../types/author-create";
import { generateToken } from "../util/generate-token";

const authorTokenRepository = ormClient.getRepository(AuthorToken);
const authorRepository = ormClient.getRepository(User);
const postRepository = ormClient.getRepository(Post);
const commentRepository = ormClient.getRepository(Comment);

class AuthService {
  login = async (login: string, password: string) => {
    try {
      const author = await authorRepository.findOne({
        where: { login },
        select: ["id", "password"],
      });
      if (!author) {
        throw new Error("User not found");
      }
      if (author.password !== password) {
        throw new Error("Password doesn't match");
      }
      const token = generateToken();
      const authorId = +author.id;
      await authorTokenRepository.save({ authorId, token });
      return token;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  registration = async (data: TCreateAuthor) => {
    try {
      await authorRepository.save({
        name: data.name,
        login: data.login,
        password: data.password,
        email: data.email,
      });
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  logout = async (userId: number) => {
    try {
      await authorTokenRepository.delete({ authorId: userId });
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  me = async (userId: number) => {
    try {
      const user = await authorRepository.findOne({ where: { id: userId } });
      return user;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  upload = async (userId: number, fileName: string) => {
    try {
      const avatar = fileName;
      await authorRepository.save({ id: userId, avatar });
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };
}

export const authService = new AuthService();
