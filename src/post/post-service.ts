import { MoreThan, Not } from "typeorm";
import { PostTag } from "../entity";
import { Comment } from "../entity/comment-entity";
import { Post } from "../entity/post-entity";
import { ormClient } from "../orm-client";
import { EPostStatus } from "../types/posts-status-enam";

const postRepository = ormClient.getRepository(Post);
const authorCommentRepository = ormClient.getRepository(Comment);
const postTagRepository = ormClient.getRepository(PostTag);

class PostService {
  list = async (
    filter: { authorId?: number; status?: EPostStatus },
    take: number = 20,
    skip: number = 0
  ) => {
    try {
      const where: any = { status: Not(EPostStatus.deleted) };
      if (filter.authorId) {
        where.authorId = filter.authorId;
      }
      if (filter.status) {
        where.status = filter.status;
      }
      const postsDb = await postRepository.find({
        where,
        take,
        skip,
        relations: ["author"],
      });
      const count = await postRepository.count({ where });
      const posts = postsDb.map(({ author, ...rest }) => ({
        ...rest,
        authorName: author.name,
      }));
      return { posts, count };
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  findOne = async (id: number) => {
    try {
      const post = await postRepository.findOne({
        where: { id },
        relations: ["author"],
      });
      if (!post) {
        throw new Error("POST NOT FOUND!!!");
      }
      const { author, ...rest } = post;
      return { ...rest, authorName: author.name };
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  save = async (
    data: { authorId: number; text: string; title: string },
    tags: number[]
  ) => {
    try {
      const post = await postRepository.save(data);
      const tagData = tags.map((tagId) => ({ tagId, postId: post.id }));
      await postTagRepository.save(tagData);
      return post.id;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  update = async (
    id: number,
    authorId: number,
    data: { title?: string; text?: string; status?: EPostStatus }
  ) => {
    try {
      await postRepository.update({ id, authorId }, data);
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  statistics = async (authorId: number) => {
    try {
      // const posts = await postRepository.count({
      //   where: { authorId, status: postStatus },
      // });
      const posts = await postRepository.find({
        where: { authorId },
        select: ["id"],
      });
      console.log(posts);
      const postedPost = posts.filter(
        (item) => item.status === EPostStatus.posted
      );
      const postedCount = +postedPost.length;

      const draftedPost = posts.filter(
        (item) => item.status === EPostStatus.draft
      );
      const draftedCount = +draftedPost.length;
      // const draft = await postRepository.count({
      //   where: { authorId, status },
      // });
      const date = new Date();
      date.setHours(date.getHours() - 24);
      const comments = await authorCommentRepository.count({
        where: {
          post: {
            authorId: authorId,
            status: Not(EPostStatus.deleted),
          },
          createdAt: MoreThan(date),
        },
      });
      return { postedCount, draftedCount, comments };
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };
}

export const postService = new PostService();
