import { Request, Response, Router } from "express";
import { authMiddleware, TAuthRequest } from "../middleware/auth-middleware";
import { checkIdMiddleware } from "../middleware/check-id-middleware";
import { checkPublishDataMiddleware } from "../middleware/check-post-data-middleware.";
import { checkUpdateDataMiddleware } from "../middleware/check-post-title-text-middleware";
import { EPostStatus } from "../types/posts-status-enam";
import { postService } from "./post-service";

export const authorPostRouter = Router();

authorPostRouter.get("/post", async (req: Request, res: Response) => {
  try {
    const take = req.query.take ? +req.query.take : 20;
    const page = req.query.page ? +req.query.page : 1;
    const skip = page > 0 ? (page - 1) * take : 0;
    const posts = await postService.list(
      { status: EPostStatus.posted },
      take,
      skip
    );
    res.send(posts);
  } catch (err) {
    res.status(400).send((err as Error).message);
  }
});

authorPostRouter.get(
  "/post/myposts",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const take = req.query.take ? +req.query.take : 20;
      const page = req.query.page ? +req.query.page : 1;
      const skip = page > 0 ? (page - 1) * take : 0;
      const authorId = (req as TAuthRequest).userId;
      const status = req.query.status as EPostStatus;
      const posts = await postService.list({ authorId, status }, take, skip);
      res.send(posts);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

authorPostRouter.get(
  "/post/:id",
  [checkIdMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id;
      const post = await postService.findOne(id);
      res.send(post);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

authorPostRouter.put(
  //update post.title, post.text
  "/post/:id",
  [authMiddleware, checkUpdateDataMiddleware],
  async (req: Request, res: Response) => {
    try {
      const data = req.body;
      const authorId = (req as TAuthRequest).userId;
      const id = +req.params.id;
      await postService.update(id, authorId, data);
      res.end();
    } catch (err) {
      res.sendStatus(400).send((err as Error).message);
    }
  }
);

authorPostRouter.put(
  // change status to "posted" or "deleted"
  "/post/:id/publish",
  [authMiddleware, checkPublishDataMiddleware, checkIdMiddleware],
  async (req: Request, res: Response) => {
    try {
      const status = req.body.status as EPostStatus;
      const authorId = (req as TAuthRequest).userId;
      const id = +req.params.id;
      await postService.update(id, authorId, { status });
      res.end();
    } catch (err) {
      res.sendStatus(400).send((err as Error).message);
    }
  }
);

authorPostRouter.post(
  //save to draft
  "/post",
  [checkUpdateDataMiddleware, authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const { tags, ...data } = req.body;
      const authorId = (req as TAuthRequest).userId;
      const id = await postService.save({ ...data, authorId }, tags);
      res.send({ id });
    } catch (err) {
      res.sendStatus(400).send((err as Error).message);
    }
  }
);

authorPostRouter.get(
  "/post/myposts/statistics",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const authorId = (req as TAuthRequest).userId;
      const data = await postService.statistics(authorId);
      res.send(data);
    } catch (err) {
      res.sendStatus(400).send((err as Error).message);
    }
  }
);
