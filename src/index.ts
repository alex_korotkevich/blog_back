require("dotenv").config();
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { authRouter } from "./auth/auth-controller";
import { commentRouter } from "./comment/comment-controller";
import { ormClient } from "./orm-client";
import { authorPostRouter } from "./post/post-controller";
import { tagRouter } from "./tag/tag-controller";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use([authorPostRouter, authRouter, commentRouter, tagRouter]);
ormClient.initialize();

app.listen(3300, () => {
  console.log("start server on 3300");
});
