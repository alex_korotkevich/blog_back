import { DataSource } from "typeorm";
import * as entities from "./entity";

export const ormClient = new DataSource({
  type: "postgres",
  username: process.env.DB_USER!,
  host: process.env.DB_HOST!,
  database: process.env.DB_DATABASE!,
  password: process.env.DB_PASSWORD!,
  port: +process.env.DB_PORT!,
  entities: Object.values(entities),
  synchronize: true,
});
