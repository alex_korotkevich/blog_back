import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class AuthorToken {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  token!: string;

  @Column()
  authorId!: number;
}
