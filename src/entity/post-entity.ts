import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { EPostStatus } from "../types/posts-status-enam";
import { Comment } from "./comment-entity";
import { Tag } from "./tag-entity";
import { User } from "./user-entity";

@Entity()
export class Post {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  authorId!: number;

  @Column()
  text!: string;

  @Column()
  title!: string;

  @Column({ default: EPostStatus.draft, enum: EPostStatus })
  status!: EPostStatus;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @ManyToOne(() => User, (user) => user.posts)
  author!: User;

  @OneToMany(() => Comment, (comment) => comment.post, {
    onDelete: "CASCADE",
  })
  comments!: Comment[];

  @ManyToMany(() => Tag, (tag) => tag.posts)
  tags!: Tag[];
}
