import { Entity, PrimaryColumn } from "typeorm";

@Entity("post_tag")
export class PostTag {
  @PrimaryColumn()
  postId!: number;

  @PrimaryColumn()
  tagId!: number;
}
