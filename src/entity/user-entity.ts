import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comment } from "./comment-entity";
import { Post } from "./post-entity";

@Entity()
export class User {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  login!: string;

  @Column({ select: false })
  password!: string;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column({ nullable: true })
  avatar?: string;

  @OneToMany(() => Post, (post) => post.author, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  posts!: Post[];

  @OneToMany(() => Comment, (comment) => comment.user, {
    onDelete: "SET NULL",
  })
  comments!: Comment[];
}
