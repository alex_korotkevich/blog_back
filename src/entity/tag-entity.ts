import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Post } from "./post-entity";

@Entity()
export class Tag {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column({ unique: true })
  text!: string;

  @ManyToMany(() => Post, (post) => post.tags)
  @JoinTable({
    name: "post_tag",
    joinColumns: [{ name: "tagId", referencedColumnName: "id" }],
    inverseJoinColumns: [{ name: "postId", referencedColumnName: "id" }],
  })
  posts!: Post[];
}
