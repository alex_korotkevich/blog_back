import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Post } from "./post-entity";
import { User } from "./user-entity";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column({ nullable: true })
  userId?: number | null;

  @Column()
  text!: string;

  @Column()
  postId!: number;

  @CreateDateColumn()
  createdAt!: Date;

  @ManyToOne(() => Post, (post) => post.comments)
  post!: Post;

  @ManyToOne(() => User, (user) => user.comments)
  user?: User | null;
}
