import { Tag } from "../entity";
import { ormClient } from "../orm-client";

const tagRepository = ormClient.getRepository(Tag);

class TagService {
  list = async (postId: number) => {
    try {
      const tags = await tagRepository.find({
        where: { posts: { id: postId } },
      });
      return tags;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  fullList = async () => {
    try {
      const tags = await tagRepository.find();
      return tags;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  excludeList = async (postId?: number) => {
    try {
      const tags = await tagRepository.find({
        where: { posts: { id: postId } },
      });
      return tags;
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  create = async (text: string) => {
    try {
      await tagRepository.save({
        text,
      });
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };
}

export const tagService = new TagService();
