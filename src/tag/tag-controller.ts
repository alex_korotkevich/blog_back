import { Request, Response, Router } from "express";
import { checkIdMiddleware } from "../middleware/check-id-middleware";
import { tagService } from "./tag-service";

export const tagRouter = Router();

tagRouter.get("/tag/exclude", async (req: Request, res: Response) => {
  try {
    const postId = req.query.postId ? +req.query.postId : undefined;
    const tags = await tagService.excludeList(postId);
    res.send(tags);
  } catch (err) {
    res.status(400).send((err as Error).message);
  }
});

tagRouter.get("/tag/tags", async (req: Request, res: Response) => {
  try {
    const tags = await tagService.fullList();
    res.send(tags);
  } catch (err) {
    res.status(400).send((err as Error).message);
  }
});

tagRouter.get(
  "/tag/:id",
  [checkIdMiddleware],
  async (req: Request, res: Response) => {
    try {
      const postId = +req.params.id;
      const tags = await tagService.list(postId);
      res.send(tags);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

tagRouter.post("/tag/create", async (req: Request, res: Response) => {
  try {
    const text = req.body.text as string;
    await tagService.create(text);
    res.end();
  } catch (err) {
    res.status(400).send((err as Error).message);
  }
});
