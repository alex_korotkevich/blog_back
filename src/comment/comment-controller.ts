import { Request, Response, Router } from "express";
import { TAuthRequest } from "../middleware/auth-middleware";
import { checkCommentTextMiddleware } from "../middleware/check-comment-text-middleware";
import { checkIdMiddleware } from "../middleware/check-id-middleware";
import { userMiddleware } from "../middleware/user-middleware";
import { commentService } from "./comment-service";

export const commentRouter = Router();

commentRouter.get(
  "/post/:id/comments",
  [checkIdMiddleware],
  async (req: Request, res: Response) => {
    try {
      const postId = +req.params.id;
      const comments = await commentService.list(postId);
      res.send(comments);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

commentRouter.post(
  "/post/:id/create",
  [checkIdMiddleware, checkCommentTextMiddleware, userMiddleware],
  async (req: Request, res: Response) => {
    try {
      const postId = +req.params.id;
      const text = req.body.text as string;
      const userId = (req as TAuthRequest).userId || null;
      const comment = await commentService.create(userId, postId, text);
      res.send(comment);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);
