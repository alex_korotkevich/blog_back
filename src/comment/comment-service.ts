import { Comment } from "../entity/comment-entity";
import { ormClient } from "../orm-client";

const commentRepository = ormClient.getRepository(Comment);

class CommentService {
  list = async (postId: number) => {
    try {
      const comments = await commentRepository.find({
        where: { postId },
        relations: ["user"],
      });
      return comments.map(({ user, ...rest }) => ({
        ...rest,
        authorName: user?.name,
      }));
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };

  create = async (userId: number | null, postId: number, text: string) => {
    try {
      const res = await commentRepository.save({
        userId,
        postId,
        text,
      });
      const comment = await commentRepository.findOne({
        where: { id: res.id },
        relations: ["user"],
      });
      const { user, ...rest } = comment!;
      return { ...rest, authorName: user?.name };
    } catch (err) {
      console.log(err);
      throw new Error("Data Base Error!!!");
    }
  };
}

export const commentService = new CommentService();
