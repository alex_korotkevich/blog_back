import { NextFunction, Request, Response } from "express";

export const checkPublishDataMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.status) {
    return res.status(400).send("Invalid status");
  }
  next();
};
