import { NextFunction, Request, Response } from "express";

export const authLoginMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.login || !data.password) {
    res.status(400).send("Invalid login or password");
  }
  if (data.login.length < 6 || data.password.length < 6) {
    res.status(400).send("Password or login too short");
  }
  next();
};
