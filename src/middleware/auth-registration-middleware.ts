import { NextFunction, Request, Response } from "express";

export const authRegistrationMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.login || !data.password || !data.name || !data.email) {
    res.status(400).send("Invalid login, password, name or email");
  }
  if (data.login.length < 6 || data.password.length < 6) {
    throw new Error("Password or login too short");
  }
  next();
};
