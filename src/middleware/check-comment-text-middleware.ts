import { NextFunction, Request, Response } from "express";

export const checkCommentTextMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.text) {
    res.status(400).send("Invalid text");
  }
  next();
};
