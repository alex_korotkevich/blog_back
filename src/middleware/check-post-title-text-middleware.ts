import { NextFunction, Request, Response } from "express";

export const checkUpdateDataMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.title || !data.text) {
    return res.status(400).send("Invalid title or text");
  }

  next();
};
