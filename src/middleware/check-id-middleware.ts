import { NextFunction, Request, Response } from "express";

export const checkIdMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = +req.params.id;
  if (!id) {
    return res.status(400).send("Invalid Id");
  }
  next();
};
