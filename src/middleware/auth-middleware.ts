import { NextFunction, Request, Response } from "express";
import { AuthorToken } from "../entity/author-token-entity";
import { ormClient } from "../orm-client";

const authorTokenRepository = ormClient.getRepository(AuthorToken);

export type TAuthRequest = Request & { userId: number };

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    return res.status(403).send("No token");
  }
  const tokenData = await authorTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    return res.status(403).send("No token data");
  }
  (req as TAuthRequest).userId = tokenData.authorId;
  next();
};
