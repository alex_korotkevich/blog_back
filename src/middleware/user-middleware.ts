import { NextFunction, Request, Response } from "express";
import { AuthorToken } from "../entity/author-token-entity";
import { ormClient } from "../orm-client";
import { TAuthRequest } from "./auth-middleware";

const authorTokenRepository = ormClient.getRepository(AuthorToken);

export const userMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    return next();
  }
  const tokenData = await authorTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    return next();
  }
  (req as TAuthRequest).userId = tokenData.authorId;
  next();
};
