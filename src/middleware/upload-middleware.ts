import busboy from "busboy";
import { NextFunction, Request, Response } from "express";
import fs from "fs";
import path from "path";
import { generateToken } from "../util/generate-token";

export type TRequest = Request & { uploadedFile: string };
export const uploadMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const stream = busboy({ headers: req.headers });
  let fileName = generateToken();

  stream.on(
    "file",
    function (
      _fieldname: string,
      file: NodeJS.ReadableStream,
      fileData: any,
      _encoding: string,
      _mimetype: string
    ) {
      const type = fileData.filename.split(".").pop();
      fileName = `${fileName}.${type}`;
      const saveTo = path.resolve(
        process.env.FRONTEND_PUBLIC_PATH || "",
        "uploads",
        fileName
      );
      file.pipe(fs.createWriteStream(saveTo));
    }
  );

  stream.on("finish", async function () {
    (req as TRequest).uploadedFile = fileName;
    next();
  });

  return req.pipe(stream);
};
