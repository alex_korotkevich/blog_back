export type TCreatePost = {
  id: number;
  authorId: number;
  text: string;
  title: string;
  status: string;
};
