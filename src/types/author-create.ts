export type TCreateAuthor = {
  name: string;
  login: string;
  password: string;
  email: string;
};
