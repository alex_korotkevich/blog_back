export type TCommentCreate = {
  id: number;
  userId?: number;
  text: string;
};
