export enum EPostStatus {
  draft = "draft",
  posted = "posted",
  deleted = "deleted",
}
